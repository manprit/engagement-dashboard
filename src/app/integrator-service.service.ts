import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { CountrySummary } from './countrysummary';
import { Observable } from 'rxjs/Observable';
import { Engagement } from './engagement';
import { User } from './user';
import { EngagementDetails } from './engagementDetails';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class IntegratorServiceService {

   constructor(private api: ApiService) { }

  
  // private countryName =  new BehaviorSubject<string>("Select Country");
   //selectedCountry = this.countryName.asObservable();
 
  // changeCountry(country:string){
    // this.countryName.next(country);
  // }
 

  getEntity(countryId: any): Observable<CountrySummary> {
   return this.api.getEntity(countryId);
  }

  getAllEntities(countryId: any): Observable<EngagementDetails> {
  console.log(countryId+">>");
    return this.api.getAllEntities(countryId);
   }

   getEntityBySBU(sbu: any): Observable<EngagementDetails> {
    console.log(sbu+">>");
      return this.api.getEntityBySBU(sbu);
     }
  

   logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
 
   
    login(username: any, password: any): Observable<User>  {
	console.log("login");
	return this.api.login(username,password);
    }
 
}
