import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { HttpModule } from '@angular/http';
import { ApiService } from './api.service';
import { IntegratorServiceService } from './integrator-service.service';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule , routingComponents } from './app-routing.module';
import { CountryDetailComponent } from './country-detail/country-detail.component';
import { AgmCoreModule } from '@agm/core';
import { AlertModule } from 'ngx-bootstrap';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule }    from '@angular/forms';
import { AuthGuard } from './_guards/auth.guard';
import { AppheaderComponent } from './appheader/appheader.component';
import { SbuDetailEngagementComponent } from './sbu-detail-engagement/sbu-detail-engagement.component';

// Import angular-fusioncharts
import { FusionChartsModule } from 'angular-fusioncharts';

// Import FusionCharts library and chart modules
import * as FusionCharts from 'fusioncharts';
import *  as FusionMaps from 'fusioncharts/fusioncharts.maps';
import * as World from 'fusioncharts/maps/fusioncharts.world';
import * as Europe from  'fusioncharts/maps/fusioncharts.europe';

import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';
import { EuropeMapComponent } from './europe-map/europe-map.component';
// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, FusionMaps, World, Europe, FusionTheme);

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    CountryDetailComponent,
    LoginComponent,
    AppheaderComponent,
    SbuDetailEngagementComponent,
    EuropeMapComponent    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FusionChartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAFgM81Qz-SwfTzUsr4F51AgDj0HdN88CQ'
    })



  ],
  providers: [ApiService, IntegratorServiceService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
