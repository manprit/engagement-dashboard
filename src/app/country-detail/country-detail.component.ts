import { Component, OnInit, NgZone} from '@angular/core';
import { CountrySummary } from '../countrysummary';
import { Country } from '../country';
import { ActivatedRoute, Router } from '@angular/router';
import { IntegratorServiceService } from '../integrator-service.service';
import { Engagement } from '../engagement';
import { EngagementDetails } from '../engagementDetails';

const countries = [
  {"name": "Germany", "countryid" : "DE"},
  {"name": "UK", "countryid" : "UK"},
  {"name": "Belgium", "countryid" : "BE"},
  {"name": "Lexumborg", "countryid" : "L"},
  {"name": "France", "countryid" : "F"},
  {"name": "US", "countryid" : "US"},
  {"name": "Switzerland", "countryid" : "CH"},
  {"name": "Portugal", "countryid" : "P"},
  {"name": "Spain", "countryid" : "E"},
  {"name": "Italy", "countryid" : "I"},
  {"name": "Norway", "countryid" : "N"},
  {"name": "India", "countryid" : "IN"},
  {"name": "Japan", "countryid" : "JAP"},
  {"name": "Hong Kong", "countryid" : "HK"},
  {"name": "Singapore", "countryid" : "SG"}
];



@Component({
  selector: 'app-country-detail',
  templateUrl: './country-detail.component.html',
  styleUrls: ['./country-detail.component.css']
})


export class CountryDetailComponent implements OnInit {

  
  entities: CountrySummary;
  country : Country;
  eDetails: EngagementDetails;
  
  constructor(private zone:NgZone, private route : ActivatedRoute , 
    private integrationSvc: IntegratorServiceService, 
    private router : Router) { 
    
    }
  
  code : any;
  countryName : any;

  ngOnInit() {
    //this.integrationSvc.selectedCountry.subscribe(countryName => this.countryName = countryName);
    
  
    
    this.code = this.route.snapshot.params['countryid'];
    console.log("mycountry" + this.code);
 
   
    this.countryName = countries.find(x => x.countryid === this.code).name;



    
    this.integrationSvc.getEntity(this.code)
      .subscribe(
        (entities) => {
          this.zone.run(() => {
          this.entities = entities;
	          console.log("countyr" +  entities);         
        });
    });

    this.integrationSvc
    .getAllEntities(this.code)
        .subscribe(
        (entities) => {
          this.eDetails = entities;
          console.log("All Entities:" + this.eDetails);
        }
      );
  }

  onSelectEngagement(country){
  console.log(this.code);
   this.router.navigate(['engagements',this.code]);
   
  }

}
