import { BnpEntity } from "./bnpEntity";
import { SbuEntity } from "./sbuEntity";

export class CountrySummary {
  revenue: string;
  totalHeadCount: string;
  bnpEntities: BnpEntity;
  sbus: SbuEntity;
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}  
  
  
  