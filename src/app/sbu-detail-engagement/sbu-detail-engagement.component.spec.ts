import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SbuDetailEngagementComponent } from './sbu-detail-engagement.component';

describe('SbuDetailEngagementComponent', () => {
  let component: SbuDetailEngagementComponent;
  let fixture: ComponentFixture<SbuDetailEngagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SbuDetailEngagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SbuDetailEngagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
