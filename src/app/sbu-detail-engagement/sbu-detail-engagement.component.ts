import { Component, OnInit, OnDestroy } from '@angular/core';
import { CountrySummary } from '../countrysummary';
import { Country } from '../country';
import { EngagementDetails } from '../engagementDetails';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { IntegratorServiceService } from '../integrator-service.service';

@Component({
  selector: 'app-sbu-detail-engagement',
  templateUrl: './sbu-detail-engagement.component.html',
  styleUrls: ['./sbu-detail-engagement.component.css']
})
export class SbuDetailEngagementComponent implements OnInit ,  OnDestroy{

  entities: CountrySummary;
  country : Country;
  eDetails: EngagementDetails;
  

  navigationSubscription;
  
  constructor(private route : ActivatedRoute , 
    private integrationSvc: IntegratorServiceService, 
    private router : Router) { 
      this.navigationSubscription = this.router.events.subscribe((e: any) => {
        // If it is a NavigationEnd event re-initalise the component
        if (e instanceof NavigationEnd) {
          this.initialiseInvites();
        }
      });
    }
    ngOnInit(){}

    initialiseInvites() {
      this.code = this.route.snapshot.params['sbuid'];    
      this.integrationSvc
      .getEntityBySBU(this.code)
          .subscribe(
          (entities) => {
            this.eDetails = entities;
            console.log("edetails" + this.eDetails);
          }
        );
    }

  code : any;
  
  ngOnDestroy() {  
    if (this.navigationSubscription) {  
       this.navigationSubscription.unsubscribe();
    }
  }
}
