import { BnpEntity } from './bnpEntity';
import { CgPoc } from './cgpoc';
import { Country } from './country';
import { Sbu } from './sbu';
import { Engagement } from './engagement';

export class EngagementDetails {
  sbus :any;
       
    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }