import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { IntegratorServiceService } from '../integrator-service.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    loginFail: boolean;

  constructor( private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
	private integrationSvc: IntegratorServiceService) { }

	

   ngOnInit() {
     this.loginFail = false;
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

	this.integrationSvc.logout();
  }
  
  get f() { return this.loginForm.controls; }
	

   onSubmit(){
      this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

  this.loading = true;
  this.router.navigate(['country']);

	/* this.integrationSvc
	      .login(this.f.username.value, this.f.password.value)
	      .subscribe(
		(user) => {
		  console.log(user);
		 // this.user = user;
		 if (user && user.errorcode =='success') {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
		    console.log("logged in");
                    localStorage.setItem('currentUser', JSON.stringify(user));
		     this.router.navigate(['country']);
                }else{
		
		}
		  this.loading = false;

		  }
	  );*/

   
   }

}
