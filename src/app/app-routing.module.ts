import { NgModule } from '@angular/core';
import {Routes, RouterModule}  from '@angular/router';
import { CountryListComponent } from './country-list/country-list.component';
import { CountryDetailComponent } from './country-detail/country-detail.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_guards/auth.guard';
import { SbuDetailEngagementComponent } from './sbu-detail-engagement/sbu-detail-engagement.component';
import { EuropeMapComponent } from './europe-map/europe-map.component';

const routes : Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    {path : 'country', component : CountryListComponent },
    {path : 'europe' , component : EuropeMapComponent },   
    {path : 'country/:countryid' , component : CountryDetailComponent },
    {path: 'login', component : LoginComponent },
    {path: 'sbudetail/:sbuid', component : SbuDetailEngagementComponent, runGuardsAndResolvers : "always"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes , {onSameUrlNavigation: "reload"})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [CountryListComponent, CountryDetailComponent, LoginComponent,SbuDetailEngagementComponent, EuropeMapComponent];
