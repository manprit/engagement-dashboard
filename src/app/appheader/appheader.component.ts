import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-appheader',
  templateUrl: './appheader.component.html',
  styleUrls: ['./appheader.component.css']
})
export class AppheaderComponent implements OnInit {
  @Input() countryName : any;
  @Input() username : any;
  
  constructor(private router : Router) { }


  ngOnInit() {
  }

  onSelectHome(){
    this.router.navigate(['country']);
  }

  onSelectSBU(sbuid){
     this.router.navigate(['sbudetail',sbuid]);     
    }
  


}
