import { TestBed, inject } from '@angular/core/testing';

import { IntegratorServiceService } from './integrator-service.service';

describe('IntegratorServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IntegratorServiceService]
    });
  });

  it('should be created', inject([IntegratorServiceService], (service: IntegratorServiceService) => {
    expect(service).toBeTruthy();
  }));
});
