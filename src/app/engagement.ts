import { BnpEntity } from './bnpEntity';
import { CgPoc } from './cgpoc';
import { Country } from './country';
import { Sbu } from './sbu';

export class Engagement {
    engagementID: number;
    active: string;
    customerLocation: string;
    deliveryBU: string;
    description: string;
    digital: string;
    endDate: string;
    engagementName: string;
    engagementRiskStatus: string;
    engagementType: string;
    headCount: string;
    model: string;
    offShoreLoc: string;
    offShoreLocCount: string;
    onShoreLocCount: string;
    rightShore: string;
    startDate: string;
    sysprojCode: string;
    tags: string;
    tcv: string;
    technology: string;
    bnpentity: BnpEntity;
    cgspoc: CgPoc;
    country: Country;
    sbu: Sbu;
  
    constructor(values: Object = {}) {
      Object.assign(this, values);
    }
  }