import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { IntegratorServiceService } from '../integrator-service.service';


const data = {
  "chart": {
    "caption": "BNPP - Europe Region",
    "nullentityfillcolor": "#01ABDC",
    "showmarkerlabels": "0",
    "showentitytooltip": "0",
    "showentityhovereffect": "0",
    "theme": "fusion"
  },  
  "markers": {
    "items": [
      {
        "id": "uk",
        "shapeid": "we-anchor",
        "x": "120.14",
        "y": "230.9",
        "label": "UK",  
        "link":"/country/UK",       
        "labelpos": "left"
      },  
      {
        "id": "Portugal",
        "shapeid": "we-anchor",
        "x": "50.14",
        "y": "360.9",
        "link":"/country/P" ,
        "label": "Portugal",
        "labelpos": "bottom"
      },         
      {
        "id": "spain",
        "shapeid": "we-anchor",
        "x": "90.14",
        "y": "380.9",
        "link":"/country/E" ,
        "label": "Spain"
      },
      {
        "id": "France",
        "shapeid": "we-anchor",
        "x": "140.14",
        "y": "305.9",
        "link":"/country/F" ,
        "label": "France"
      },
      {
        "id": "germany",
        "shapeid": "we-anchor",
        "x": "200.14",
        "y": "220.9",
        "link":"/country/DE" ,
        "label": "Germany"
      },
      {
        "id": "Belgium",
        "shapeid": "we-anchor",
        "x": "155.14",
        "y": "245.9",
        "link":"/country/BE" ,
        "label": "Belgium"
      },
      {
        "id": "Switzerland",
        "shapeid": "we-anchor",
        "x": "185.14",
        "y": "285.9",
        "link":"/country/CH" ,
        "label": "Switzerland",
        "labelpos": "bottom"
      },
      
      {
        "id": "Italy",
        "shapeid": "we-anchor",
        "x": "220.14",
        "y": "320.9",
        "link":"/country/I" ,
        "label": "Italy",
        "labelpos": "bottom"
      },
      {
        "id": "Norway",
        "shapeid": "we-anchor",
        "x": "180.14",
        "y": "80.9",
        "link":"/country/N" ,
        "label": "Norway",
        "labelpos": "bottom"
      }   
     
    ],
    "shapes": [
      {
        "id": "we-anchor",
        "type": "image",
        "url": "https://cdn3.iconfinder.com/data/icons/iconic-1/32/map_pin_fill-512.png",
        "xscale": "8",
        "yscale": "8"
      }
    ]
  }
};

@Component({
  selector: 'app-europe-map',
  templateUrl: './europe-map.component.html',
  styleUrls: ['./europe-map.component.css']
})


export class EuropeMapComponent implements OnInit { 
  width = 350;
  height = 500;
  type = 'europe';
  dataFormat = 'json';
  dataSource = data;
  zoom: number = 2;
  username :any;
  selectedOption : any;

countries = [
  {"name": "Germany", "countryid" : "DE"},
  {"name": "UK", "countryid" : "UK"},
  {"name": "Belgium", "countryid" : "BE"},
  {"name": "Lexumborg", "countryid" : "L"},
  {"name": "France", "countryid" : "F"},
  {"name": "US", "countryid" : "US"},
  {"name": "Switzerland", "countryid" : "CH"},
  {"name": "Portugal", "countryid" : "P"},
  {"name": "Spain", "countryid" : "E"},
  {"name": "Italy", "countryid" : "I"},
  {"name": "Norway", "countryid" : "N"},
  {"name": "India", "countryid" : "IN"},
  {"name": "Japan", "countryid" : "JAP"},
  {"name": "Hong Kong", "countryid" : "HK"},
  {"name": "Singapore", "countryid" : "SG"}
];

constructor(private router : Router,   private integrationSvc: IntegratorServiceService, zone:NgZone ){}

countryName : any;

ngOnInit() {
 // this.integrationSvc.selectedCountry.subscribe(countryName => this.countryName = countryName);
  this.username = "Ashish Athawale";
}
 
onBack(){
  this.router.navigate(['country']);
}

}
