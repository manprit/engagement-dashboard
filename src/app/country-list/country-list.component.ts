import { Component, OnInit, Input, NgZone } from '@angular/core';
import { CountrySummary } from '../countrysummary';
import {ActivatedRoute, Params, Router} from '@angular/router';
import { IntegratorServiceService } from '../integrator-service.service';
import {AgmCoreModule} from '@agm/core';

const data = {
  "chart": {
    "caption": "BNPP - Locations",
    "nullentityfillcolor": "#01ABDC",
    "showmarkerlabels": "0",
    "showentitytooltip": "0",
    "showentityhovereffect": "0",
    "theme": "fusion"
  },  
  "markers": {
    "items": [
      {
        "id": "atl",
        "shapeid": "we-anchor",
        "x": "150.14",
        "y": "140.9",
        "label": "USA",  
        "link":"/country/US",      
        "labelpos": "left"
      },
      {
        "id": "uk",
        "shapeid": "we-anchor",
        "x": "350.14",
        "y": "125.9",
        "label": "Europe",  
        //"link":"/country/UK",
        "link": "/europe",
        "labelpos": "left"
      },
      {
        "id": "ind",
        "shapeid": "we-anchor",
        "x": "500.14",
        "y": "203.9",
        "label": "India",  
        "link":"/country/IN",   
        "labelpos": "bottom"
      },     
      {
        "id": "Sing",
        "shapeid": "we-anchor",
        "x": "560.14",
        "y": "231.9",
        "link":"/country/SG",
        "label": "Singapore"
        
      },     
      {
        "id": "jap",
        "shapeid": "we-anchor",
        "x": "633.14",
        "y": "145.9",
        "link":"/country/JAP",
        "label": "Japan"
       
      },  
      {
        "id": "HongKong",
        "shapeid": "we-anchor",
        "x": "550.14",
        "y": "180.9",
        "link":"/country/HK" ,
        "label": "Hong Kong",
        "labelpos": "bottom"
      }      
    ],
    "shapes": [
      {
        "id": "we-anchor",
        "type": "image",
        "url": "https://cdn3.iconfinder.com/data/icons/iconic-1/32/map_pin_fill-512.png",
        "xscale": "8",
        "yscale": "8"
      }
    ]
  }
};




@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit { 
  
  //maps
  
     width = 350;
    height = 450;
    type = 'world';
    dataFormat = 'json';
    dataSource = data;
    zoom: number = 2;
    username :any;
    selectedOption : any;

  countries = [
    {"name": "Germany", "countryid" : "DE"},
    {"name": "UK", "countryid" : "UK"},
    {"name": "Belgium", "countryid" : "BE"},
    {"name": "Lexumborg", "countryid" : "L"},
    {"name": "France", "countryid" : "F"},
    {"name": "US", "countryid" : "US"},
    {"name": "Switzerland", "countryid" : "CH"},
    {"name": "Portugal", "countryid" : "P"},
    {"name": "Spain", "countryid" : "E"},
    {"name": "Italy", "countryid" : "I"},
    {"name": "Norway", "countryid" : "N"},
    {"name": "India", "countryid" : "IN"},
    {"name": "Japan", "countryid" : "JAP"},
    {"name": "Hong Kong", "countryid" : "HK"},
    {"name": "Singapore", "countryid" : "SG"}
  ];

  constructor(private router : Router,   private integrationSvc: IntegratorServiceService, zone:NgZone ){}
  
  countryName : any;
  
  ngOnInit() {
   // this.integrationSvc.selectedCountry.subscribe(countryName => this.countryName = countryName);
    this.username = "Ashish Athawale";
  }
  onSelectCountry(county){
   // this.integrationSvc.changeCountry(county.name);
    //this.router.navigate(['country',county.countryid]);
    this.router.navigate(['country',county]);
  }

    clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
    this.router.navigate(['country', `${label || index}` ]);
  }
  
  mapClicked($event: MouseEvent) {
   
  }
  
  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

markers: marker[] = [
      {
          lat: 20.5937,
          lng: 78.9629,
          label: 'IN',
          draggable: true,
      },
      {
          lat: 55.3781,
          lng: 3.4360,
          label: 'UK',
          draggable: false
	  
      },
      {
          lat: 51.1657,
          lng: 10.4515,
          label: 'DE',
          draggable: false
       },
      {
          lat: 51.1657,
          lng: 10.4515,
          label: 'DE',
          draggable: false
      },
      {
          lat: 50.5039,
          lng: 4.4699,
          label: 'BE',
          draggable: false
      },
      {
          lat: 49.8153,
          lng: 6.1296,
          label: 'L',
          draggable: false

      },
      {
          lat: 46.2276,
          lng: 2.2137,
          label: 'F',
          draggable: false
      },
      {
          lat: 37.0902,
          lng: 95.7129,
          label: 'US',
          draggable: false
      },
      {
          lat: 46.8182,
          lng: 8.2275,
          label: 'CH',
          draggable: false
      },
      {
          lat: 39.3999,
          lng: 8.2245,
          label: 'P',
          draggable: false
      },
      {
          lat: 40.4637,
          lng: 3.7492,
          label: 'E',
          draggable: false
      },
      {
          lat: 41.8719,
          lng: 12.5674,
          label: 'I',
          draggable: false
      },
      {
          lat: 60.4720,
          lng: 8.4689,
          label: 'N',
          draggable: false
      },
      {
          lat: 36.2048,
          lng: 138.2529,
          label: 'JAP',
          draggable: false
      },
      {
          lat: 22.3964,
          lng: 114.1095,
          label: 'HK',
          draggable: false
      },
      {
          lat: 1.3521,
          lng: 1103.8198,
          label: 'SG',
          draggable: false
      }
  ]
}



// just an interface for type safety.
interface marker {
    lat: number;
    lng: number;
    label?: string;
    draggable: boolean;
}