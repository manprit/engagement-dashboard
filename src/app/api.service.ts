import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment.prod';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { CountrySummary } from './countrysummary';
import { User } from './user';
import { EngagementDetails } from './engagementDetails';
import { BehaviorSubject } from 'rxjs';

const API_URL = environment.apiUrl;
const API_URL1 = environment.apiUrl1;
const API_URL2 = environment.apiurl2;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor( private http: Http) { }

  public getEntity(countryId: any): Observable<CountrySummary> {
    return this.http
    .get(API_URL1 + '/summary')
    //  .get(API_URL + '/engagements/countrySummary/'+countryId)
      .map(response => {
        const entites = response.json();
        console.log(entites);
       return  new CountrySummary(entites);
       //return entites.map((entity) => new CountrySummary(entity));
      })
      .catch(this.handleError);
  }

  public getAllEntities(countryId: any): Observable<EngagementDetails> {
   console.log(countryId + '>>');
    return this.http
    .get(API_URL + '/engengagement-detail')
     // .get(API_URL + '/engagements/' + countryId)
      .map(response => {
        const entity = response.json();
        console.log(entity);
        return new EngagementDetails(entity);
      })
      .catch(this.handleError);
  }

  public getEntityBySBU(sbu: any): Observable<EngagementDetails> {
    console.log(sbu + '>>>>');
     return this.http
     .get(API_URL2 + '/sbudetail')
      // .get(API_URL + '/engagements/' + countryId)
       .map(response => {
         const entity = response.json();
         console.log(entity);
         return new EngagementDetails(entity);
       })
       .catch(this.handleError);
   }
 

  
  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

 public login(username: any, password: any) : Observable<User>{
    	     return this.http.get(API_URL +'/auth/authenticate?username='+username+'&password='+password)
      .map(response => {
        const user = response.json();
        console.log(user);
       return  new User(user);
       //return entites.map((entity) => new CountrySummary(entity));
      })
      .catch(this.handleError);
    }
}
